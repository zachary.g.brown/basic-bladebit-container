FROM ubuntu:22.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y build-essential cmake libgmp-dev libnuma-dev git ubuntu-drivers-common wget && apt clean

CMD tail -f /dev/null
